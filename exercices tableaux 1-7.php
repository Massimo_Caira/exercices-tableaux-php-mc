<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Exercices tableaux Massimo</title>
</head>
<body>
<?php
/*
Exercice 1:
Afficher ci-dessous dans la page le jour de la semaine ayant pour index 5.
Utiliser une balise HTML paragraphe. */

echo '<h2>Exercice 1</h2>';

$joursSemaine = ['Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi', 'Dimanche'];

echo "<p>$joursSemaine[5]</p>";

/**
 * Exercice 2:
 * Grâce à la fonction date(), on récupère dans la variable $indexAujourdhui l'index de 0 à 6 du jour actuel de la semaine.
 * Utiliser la fonction echo(), le tableau $joursSemaine et la variable $indexAujourdhui pour afficher le jour actuel.
 * Utiliser une balise HTML paragraphe.
 */
echo '<h2>Exercice 2</h2>';
echo PHP_EOL;
$joursSemaine2 = ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'];
$indexAujourdhui = date("w");
echo "<p>$joursSemaine2[$indexAujourdhui]</p>";


/**
 * Exercice 3:
 * Pour vérifier ce que contient un tableau, on peut utiliser les fonctions print_r(); ou var_dump();
 * https://www.php.net/manual/fr/function.print-r.php
 * https://www.php.net/manual/fr/function.var-dump.php
 * A l'aide d'une de ces deux fonctions, afficher le contenu du tableau $joursSemaine dans une balise HTML pre.
 */
echo '<h2>Exercice 3</h2>';
echo PHP_EOL;
echo '<pre>'.var_dump($joursSemaine).'</pre>';


/**
 * Exercice 4:
 * Les weekends ne sont pas assez longs !!
 * Dans le tableau $joursSemaine, remplacer la valeur "Lundi" par "Encore dimanche", à l'aide de son index et en
 * utilisant un opérateur d'affectation.
 * exemple de syntaxe: $monTableau[123] = "Nouvelle valeur"; // affecte la valeur "Nouvelle valeur" à l'index 123
 * du tableau $monTableau
 */
echo '<h2>Exercice 4</h2>';
echo PHP_EOL;

$joursSemaine[0] = "Encore dimanche";

/**
 * Exercice 5:
 * Répéter l'exercice 3 pour vérifier que l'exercice 4 est correctement réalisé.
 */
echo '<h2>Exercice 5</h2>';
echo PHP_EOL;
echo '<pre>'.var_dump($joursSemaine).'</pre>';

/**
 * Exercice 6:
 * A l'aide d'une boucle for, parcourir le tableau et afficher l'ensemble de ses valeurs dans une liste à puces (balises
 * HTML ul et li).
 * Vous pouvez utiliser la fonction count() pour compter le nombre d'element d'un tableau.
 */
echo '<h2>Exercice 6</h2>';
echo PHP_EOL;
echo '<ul>';
for ($i = 0 ;$i <7 ; $i++) {
    echo '<li>'.$joursSemaine[$i].'</li>';
}
echo '</ul>';

/**
 * Exercice 7:
 * Répétez l'exercice 6 à l'aide d'une boucle foreach :
 * foreach fournit une façon simple de parcourir des tableaux et permet de répéter l'execution d'une portion de code
 * pour chaque valeur d'un tableau
 *   foreach ($monTableau as $maValeur){
 *     // code à répéter
 *   }
 * pour en savoir plus: https://www.php.net/manual/fr/control-structures.foreach.php
 */
echo '<h2>Exercice 7</h2>';
echo PHP_EOL;
echo '<ul>';
foreach ( $joursSemaine AS $jour ) {
    echo '<li>'.$jour.'</li>';
}
echo '</ul>';

?>

</body>
</html>

