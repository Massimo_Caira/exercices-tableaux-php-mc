<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<?php
$tableDe = 4;
$nombres = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

/**
 * Exercice 11:
 * à l'aide de la boucle foreach parcourir le tableau $nombres pour afficher la table de multiplication de la valeur
 * contenue dans la variable $tableDe.
 * Chaque ligne du résultat généré doit ressembler à l'extrait suivant : "4 * 1 = 4"
 */
echo '<h2>Exercice 11</h2>';
echo PHP_EOL;
$table = array();
foreach ($tableDe as $values){
$tab = array();

foreach($table as $val) {
    $tab [] = $values * $val;
}
    $table[] = $tab;
}

?>
</body>
</html>